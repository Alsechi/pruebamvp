package com.logisticapp.loginpruebamvp;

public interface LoginInterface {

    //operaciones del model que necesita el presenter
    interface OnloginFinishedlistiner{

        void onUsernameError();
        void onPasswordError();
        void onSuccess();

    }

    void Login(String username,String Password,OnloginFinishedlistiner listener);

}
