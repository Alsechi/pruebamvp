package com.logisticapp.loginpruebamvp;

public interface LoginPresenter {

    void validateCredentials (String username,String password);

    void onDestroy();

}
