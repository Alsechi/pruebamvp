package com.logisticapp.loginpruebamvp;

public interface MainPresenter {
    void onResume();

    void onItemClicked(int position);

    void onDestroy();
}
