package com.logisticapp.loginpruebamvp;

import java.util.List;

public class MainPresenterImpl implements MainPresenter,FindItemsInteractor.OnFinishedListener {
    private Mainview mainView;
    private FindItemsInteractor findItemsInteractor;

    public MainPresenterImpl(Mainview mainView, FindItemsInteractor findItemsInteractor) {
        this.mainView = mainView;
        this.findItemsInteractor = findItemsInteractor;
    }

    @Override public void onResume() {
        if (mainView != null) {
            mainView.showProgress();
        }

        findItemsInteractor.findItems(this);
    }

    @Override public void onItemClicked(int position) {
        if (mainView != null) {
            mainView.showMessage(String.format("Position %d clicked", position + 1));
        }
    }

    @Override public void onDestroy() {
        mainView = null;
    }

    @Override public void onFinished(List<String> items) {
        if (mainView != null) {
            mainView.setItems(items);
            mainView.hideProgress();
        }
    }

    public Mainview getMainView() {
        return mainView;
    }
}
