package com.logisticapp.loginpruebamvp;

import java.util.List;

public interface Mainview {
    void showProgress();

    void hideProgress();

    void setItems(List<String> items);

    void showMessage(String message);
}
